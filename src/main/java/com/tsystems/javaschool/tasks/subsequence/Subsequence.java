package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int n = 0;
        boolean b = false;
        if(x==null || y==null){
            throw new IllegalArgumentException();
        }
        if (x.size()==0){
            return true;
        }
        else if( y.size()==0 ){
            return b;
        }



            for (Object s : y) {
                String s1 = s.toString();

                if (s == x.get(n)) {
                    if (n == x.size() - 1) {
                        b = true;
                        break;
                    }
                    n++;
                }
            }

            return b;
    }
}