package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.size() < Integer.MAX_VALUE-1) {
            try {
                int[] arr = new int[inputNumbers.size()];
                for (Integer elem : inputNumbers) {
                    if (elem == null) {
                        throw new CannotBuildPyramidException();
                    }
                }
                for (int i = 0; i < inputNumbers.size(); i++) arr[i] = inputNumbers.get(i);
                Arrays.sort(arr);

                boolean f = true;
                int row = 2;
                int i = 3;
                while (f) {
                    int length = arr.length;
                    if (i == length) {
                        break;
                    } else {
                        row++;
                        i += row;
                        if (i > length) {
                            f = false;
                            break;
                        }
                    }
                }
                int[][] array = new int[row][row * 2 - 1];
                if (f) {
                    array = new int[row][row * 2 - 1];
                    int k = (int) ((row * 2 - 1) / 2 + 0.5);
                    array[0][k] = arr[0];
                    int a = 1;
                    for (int q = 1; q < row; q++) {
                        int po = --k;
                        for (int j = 0; j < q + 1; j++) {
                            array[q][po] = arr[a];
                            po += 2;
                            a++;
                        }
                    }
                    for (int i1 = 0; i1 < array.length; i1++) {
                        for (int j = 0; j < array[i1].length; j++) {
                            System.out.print(array[i1][j] + "\t");
                        }
                        System.out.println();
                    }
                } else throw new CannotBuildPyramidException();
                return array;
            } catch (Exception e) {
                throw new CannotBuildPyramidException();
            }

        } else throw new CannotBuildPyramidException();
    }
}

